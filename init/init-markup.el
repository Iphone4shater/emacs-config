;;; init-markup.el --- Configuration for various markup language modes -*- lexical-binding:t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)

(package-require 'json-mode)
(package-require 'yaml-mode)
(package-require 'flymake-yaml)
(package-require 'jinja2-mode)

(package-require 'csv-mode)
(package-require 'markdown-mode)
(package-require 'markdown-toc)

(provide 'init-markup)

;;; init-markup.el ends here
