;;; init-yas.el --- Snippet engine setup -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)

(package-require 'yasnippet)
(package-require 'yasnippet-snippets)

(yas-global-mode t)

(provide 'init-yas)

;;; init-yas.el ends here
