;;; ya.el --- Transient integration for ya -*- lexical-binding: t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1.1
;; Package-Requires: (transient)

;;; Commentary:

;; This package provides a `transient' wrapper for Yandex's primary
;; development tool, ya.

;;; Code:

(require 'transient)

(defvar ya-executable nil);; "/home/igorbalduev/rep/arcadia/workspace/ya"

(defun ya--execute (args)
  "Execute ya command with ARGS."
  (unless ya-executable
    (error "Executable path is not set"))
  (compile (string-join (append (list ya-executable) args) " ")))

(defun ya--make (dirs &optional args)
  "Execute ya make command in DIRS with ARGS."
  (interactive (list (read-directory-name "Project to build: " default-directory nil t)
                     (transient-args 'ya-transient)))
  (ya--execute (append (list "make") args (list dirs))))

(defun ya--generate (dirs &optional args)
  "Use ya make with ARGS to emit generated files in DIRS."
  (interactive (list (read-directory-name "Project to build: " default-directory nil t)
                     (append (transient-args 'ya-transient)
                             (list
                              "--add-result .h --add-result .hh --add-result .hpp --add-result .inc --add-result .c --add-result .cc --add-result .cpp --force-build-depends --replace-result --keep-going"))))
  (ya--execute (append (list "make") args (list dirs))))

(defun ya--compile-commands (dirs &optional args)
  "Generate clangd's compile-commands.json for projects under DIRS with ARGS."
  (interactive (list (read-directory-name "Root to generate compile-commands.json: "
                                          default-directory nil t)
                     (transient-args 'ya-transient--compile-commands)))
  (ya--execute (append (list "ide" "vscode-clangd" "--use-arcadia-root") args (list dirs))))

(defun ya--output-reader (prompt initial-input &rest args)
  "Prompt for output directory using PROMPT with INITIAL-INPUT (ignore ARGS)."
  (ignore args)
  (read-directory-name prompt default-directory initial-input t))

(transient-define-infix ya-transient--build ()
  :description "Build type"
  :class 'transient-option
  :shortarg "-b"
  :argument "--build="
  :choices '("debug" "relwithdebinfo" "release"))

(transient-define-infix ya-transient--sanitize ()
  :description "Sanitizer type"
  :class 'transient-option
  :shortarg "-s"
  :argument "--sanitize="
  :choices '("address" "memory" "thread" "undefined" "leak"))

(transient-define-infix ya-transient--rebuild ()
  :description "Rebuld all"
  :class 'transient-switch
  :shortarg "-r"
  :argument "--rebuild")

(transient-define-infix ya-transient--no-src-links ()
  :description "Do not create any symlink in source directory"
  :class 'transient-switch
  :shortarg "-l"
  :argument "--no-src-links")

(transient-define-infix ya-transient--output ()
  :description "Directory with build results"
  :class 'transient-option
  :shortarg "-o"
  :argument "--output="
  :reader #'ya--output-reader)

(transient-define-infix ya-transient--project-output ()
  :description "Directory with project creation results"
  :class 'transient-option
  :shortarg "-p"
  :argument "--project-output="
  :reader #'ya--output-reader)

(transient-define-infix ya-transient--yt-store ()
  :description "Use YT storage"
  :class 'transient-switch
  :shortarg "-y"
  :argument "--yt-store")

(transient-define-infix ya-transient--tests ()
  :description "Run tests"
  :class 'transient-switch
  :shortarg "-t"
  :argument "-tt")

(transient-define-infix ya-transient--tidy ()
  :description "Include clang-tidy checks into the test run"
  :class 'transient-switch
  :shortarg "-d"
  :argument "-DTIDY")

(transient-define-infix ya-transient--coverage ()
  :description "Collect coverage data for C++ code"
  :class 'transient-switch
  :shortarg "-c"
  :argument "--clang-coverage")

(transient-define-infix ya-transient--canonize ()
  :description "Canonize tests"
  :class 'transient-switch
  :shortarg "-z"
  :argument "--canonize-tests")

(transient-define-infix ya-transient--threads ()
  :description "Threads count"
  :class 'transient-option
  :shortarg "-j"
  :argument "--threads=")

(transient-define-infix ya-transient--test-filter ()
  :description "Test filter"
  :class 'transient-option
  :shortarg "-f"
  :argument "-F=")

(transient-define-prefix ya-transient--compile-commands ()
  "IDE support."
  ["Options"
   (ya-transient--project-output)
   (ya-transient--threads)]
  ["Actions"
   ("m" "Create compile-commands.json" ya--compile-commands)])

(transient-define-prefix ya-transient ()
  "Ya Make Tool."
  :value '("--build=relwithdebinfo" "--no-src-links" "--yt-store" "-tt" "-DTIDY")
  ["Options"
   (ya-transient--build)
   (ya-transient--sanitize)
   (ya-transient--rebuild)
   (ya-transient--no-src-links)
   (ya-transient--yt-store)
   (ya-transient--tests)
   (ya-transient--tidy)
   (ya-transient--output)
   (ya-transient--coverage)
   (ya-transient--canonize)
   (ya-transient--test-filter)]
  ["Actions"
   ("m" "Make project" ya--make)
   ("g" "Create generated files" ya--generate)]
  ["Subcommands"
   ("i" "IDE support" ya-transient--compile-commands)])

(provide 'ya)

;;; ya.el ends here
