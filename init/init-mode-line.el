;;; init-mode-line.el --- Mode and header line configuration -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq mode-line-compact t)

(require 'auto-package)

(package-require 'headlock)

(provide 'init-mode-line)

;;; init-mode-line.el ends here
