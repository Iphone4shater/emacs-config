;;; init-os.el --- Setup and configuration for OS-specific features -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'proced)
(require 'auto-package)

(package-require 'vterm)
(package-require 'vterm-toggle)

(require 'hl-line)
(defun my-vterm-hook ()
  "Hook to tune vterm properties."
  (when (or hl-line-mode global-hl-line-mode)
    (hl-line-mode 'toggle)))

(add-hook 'vterm-mode-hook #'my-vterm-hook)

;; Process explorer settings
(defun my-proced-auto-update ()
  "Customization for proced."
  (proced-toggle-auto-update 1))

(with-eval-after-load 'proced
  (add-hook 'proced-mode-hook 'my-proced-auto-update))

;; Virtual terminal setup and configuration
(setq vterm-shell "/bin/zsh"
      vterm-toggle-fullscreen-p t)

(keymap-global-set "C-c t" #'vterm-toggle)

;; Minor mode for systemd unit files
(package-require 'systemd)

;; Handy privilege elevation command
(package-require 'sudo-edit)

;; Clipboard support for TTY mode
(unless (display-graphic-p)
  (package-require 'clipetty))

(require 'dired)
(setq dired-dwim-target t)

(provide 'init-os)

;;; init-os.el ends here
