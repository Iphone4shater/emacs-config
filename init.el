;;; init.el --- My personal Emacs config entry file -*- lexical-binding:t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1

;;; Commentary:

;; This is my personal Emacs configuration, organized as a set of local packages
;; to simplify management and encourage myself to use elisp more frequently.

;;; Code:
(require 'early-init)

(eval-and-compile
  (add-to-list 'load-path
	           (expand-file-name "init" user-emacs-directory)))

(load custom-file t)

(require 'flymake)

(defun my-elisp-flymake-byte-compile-with-load-path (orig &rest args)
  "Make Flymake honor load path by advising on ORIG with ARGS."
  (let ((elisp-flymake-byte-compile-load-path
	     (append elisp-flymake-byte-compile-load-path load-path)))
    (apply orig args)))

(eval-when-compile
  (advice-add 'elisp-flymake-byte-compile
	          :around #'my-elisp-flymake-byte-compile-with-load-path))

(require 'init-package-archives)

(keymap-global-unset "C-x C-c")
(keymap-global-set "<remap> <kill-buffer>" #'kill-current-buffer)
(keymap-global-set "<remap> <list-buffers>" #'ibuffer)

;; Miscellaneous settings
(require 'init-os)
(require 'init-project)
(require 'init-completion)

(require 'init-visual)
(require 'init-mode-line)
(require 'init-security)

(require 'init-text)
(require 'init-yas)
(require 'init-tree-sitter)

;; Major mode-specific settings
(require 'init-misc)
(require 'init-markup)
(require 'init-prog)
(require 'init-eglot)
(require 'init-lisp)
(require 'init-python)
(require 'init-sql)
(require 'init-cc)

(load (expand-file-name "secret.el" user-emacs-directory) t)

(provide 'init)
;;; init.el ends here
