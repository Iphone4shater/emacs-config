;;; init-project.el --- Project and VC settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)
(require 'project)

(package-require 'project-rootfile)
(package-require 'diff-hl)

(require 'diff-hl-margin)

(if (display-graphic-p)
    (global-diff-hl-mode t)
  (diff-hl-margin-mode t))

(add-to-list 'project-rootfile-list ".phony-project")
(add-to-list 'project-find-functions #'project-rootfile-try-detect t)

(require 'init-os)

(setq vterm-toggle-scope 'project
      vterm-toggle-project-root t)

(customize-set-value 'project-switch-commands
                     '((project-switch-to-buffer "Opened buffer")
                       (project-find-file "Find file")
                       (project-find-regexp "Find regexp")
                       (project-find-dir "Find directory")
                       (project-vc-dir "VC-Dir")))

(provide 'init-project)
;;; init-project.el ends here
