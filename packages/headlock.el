;;; headlock.el --- Minor mode for showing current path in a headline -*- lexical-binding: t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1.3
;; Package-Requires: (cl-lib project)

;;; Commentary:

;;; Code:

(require 'cl-lib)
(require 'treesit)
(require 'project)

(defgroup headlock nil
  "Show file path and defun name at point in a header."
  :group 'lisp)

(defcustom headlock-idle-delay 0.5
  "Number of seconds of idle time to wait before updating headlock."
  :type 'number)

(defvar-local headlock-buffer-file-name nil
  "Buffer file name with directory names collapsed to their first letters.")

(defvar headlock-info nil
  "Header line component to render on update.")
(put 'headlock-info 'risky-local-variable t)

(defvar headlock-timer nil
  "Header line update timer.")

(defun headlock--shorten-path (path)
  "Shorten a PATH by keeping only first letters for directories."
  (let* ((elems (file-name-split path))
         (filename (last elems)))
    (when (string-empty-p (car elems))
      (setcar elems "/"))

    (cl-flet ((first-letter (str) (substring str 0 1)))
      (apply #'file-name-concat
             (append (mapcar #'first-letter
                             (nbutlast elems))
                     filename)))))

(defun headlock-simple-treesit-trace (pos)
  "Renders a source code path starting at POS."
  (propertize
   (treesit-defun-name
    (if (equal pos (point))
        (treesit-defun-at-point)
      (save-excursion
        (goto-char pos)
        (treesit-defun-at-point))))
   'face 'header-line-highlight))

(defvar headlock-path-trace-function #'headlock-simple-treesit-trace
  "Function that renders the source code path at point.")

(defun headlock--update ()
  "Update current defun and refresh header line."
  (let ((non-essential t))
    (setq headlock-info (list (funcall headlock-path-trace-function (point))))

    (when headlock-buffer-file-name
      (push (propertize
             (concat headlock-buffer-file-name ":") 'face 'mode-line-inactive)
            headlock-info))

    (force-mode-line-update)))

(defun headlock--initialize ()
  "Initialize `headlock-mode' upon enabling."
  (when buffer-file-name
    (setq headlock-buffer-file-name
          (headlock--shorten-path
           (file-relative-name buffer-file-name
                               (project-root (project-current))))))

  (set-face-attribute
   'header-line nil
   :foreground 'unspecified
   :background 'unspecified
   :inherit 'mode-line)
  (set-face-attribute
   'header-line-highlight nil
   :foreground 'unspecified
   :background 'unspecified
   :inherit 'mode-line-highlight)
  (setq header-line-format '("" headlock-info))

  (add-hook
   'post-command-hook #'headlock-schedule-timer nil :local))

(defun headlock--cleanup ()
  "Cleanup after `headlock-mode' on disabling."
  (setq header-line-format nil)
  (mapc #'kill-local-variable
        '(headlock-buffer-file-name headlock--active-defun-node)))

;;;###autoload
(define-minor-mode headlock-mode
  "Minor mode to show file path and defun name at point in a header."
  :init-value nil
  (if headlock-mode
      (headlock--initialize)
    (headlock--cleanup)))

(defun headlock-schedule-timer ()
  "(Re)schedule header line update timer."
  (unless (and headlock-timer
               (memq headlock-timer timer-idle-list))
    (setq headlock-timer
          (run-with-idle-timer
           headlock-idle-delay nil
           (lambda ()
             (when headlock-mode
               (with-demoted-errors "headlock error: %s"
                 (headlock--update))))))))


(provide 'headlock)

;;; headlock.el ends here
