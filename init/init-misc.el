;;; init-misc.el --- Miscellaneous modes setup and configuration -*- lexical-binding:t -*-
;;; Commentary:
;;; Code:

(require 're-builder)
(setq reb-re-syntax #'string)

(require 'auto-package)

(package-require 'vlf)
(package-require 'protobuf-mode)
(package-require 'lua-mode)
(package-require 'bazel)

(provide 'init-misc)

;;; init-misc.el ends here
