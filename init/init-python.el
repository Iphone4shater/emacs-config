;;; init-python.el --- Configuration for Python programming language -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)

(package-require 'python-black)

(provide 'init-python)

;;; init-python.el ends here
