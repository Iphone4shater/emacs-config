;;; init-package-archives.el --- Configure public package archives -*- lexical-binding:t -*-
;;; Commentary:
;;; Code:

(require 'package-x)

(defvar my-package-archive-dir
  (expand-file-name "packages/" user-emacs-directory)
  "Location of personal package source files.")

(defvar my-local-archive-dir
  (expand-file-name "local/" user-emacs-directory)
  "Location of the personal package archive.")

(setq package-archive-upload-base my-local-archive-dir
      package-archives
      `(("gnu" . "https://elpa.gnu.org/packages/")
	    ("melpa" . "https://melpa.org/packages/")
	    ("jsc-elpa" . "https://jcs-emacs.github.io/jcs-elpa/packages/")
	    ("local" . ,my-local-archive-dir))
      package-archive-priorities
      '(("local" . 100)
	    ("gnu" . 90)
	    ("melpa" . 80)
	    ("jsc-elpa" . 50)))

(unless (file-directory-p my-local-archive-dir)
  (make-directory my-local-archive-dir)
  (mapc #'package-upload-file
        (seq-filter #'file-regular-p
                    (directory-files my-package-archive-dir t))))

(package-initialize)

(unless (package-installed-p 'auto-package)
  (package-refresh-contents)
  (package-install 'auto-package))

(provide 'init-package-archives)

;;; init-package-archives.el ends here
