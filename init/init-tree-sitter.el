;;; init-tree-sitter.el --- Configuration of the tree-sitter feature -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'cl-lib)

(defun my-ts--find-first-of (&rest names)
  "Return a path to the first reachable executable from NAMES."
  (seq-some #'executable-find names))

(defvar my-ts-c-compiler
  (my-ts--find-first-of "clang" "gcc" "cc")
  "Path to the selected C compiler.")

(defvar my-ts-cpp-compiler
  (my-ts--find-first-of "clang++" "g++" "c++")
  "Path to the selected C++ compiler.")

(defvar my-ts-language-source-alist
  '((bash . ("https://github.com/tree-sitter/tree-sitter-bash" "v0.20.5"))
    (c . ("https://github.com/tree-sitter/tree-sitter-c" "v0.20.7"))
    (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp" "v0.22.0"))
    (commonlisp . ("https://github.com/tree-sitter-grammars/tree-sitter-commonlisp" "v0.3.3"))
    (cmake . ("https://github.com/uyha/tree-sitter-cmake" "v0.4.1"))
    (css . ("https://github.com/tree-sitter/tree-sitter-css" "v0.21.0"))
    (dockerfile . ("https://github.com/camdencheek/tree-sitter-dockerfile" "v0.1.2"))
    (go . ("https://github.com/tree-sitter/tree-sitter-go" "v0.20.0"))
    (html . ("https://github.com/tree-sitter/tree-sitter-html" "v0.20.1"))
    (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "v0.21.2"))
    (json . ("https://github.com/tree-sitter/tree-sitter-json" "v0.21.0"))
    (lua . ("https://github.com/tree-sitter-grammars/tree-sitter-lua" "v0.1.0"))
    (markdown . ("https://github.com/tree-sitter-grammars/tree-sitter-markdown" "v0.2.3"))
    (perl . ("https://github.com/ganezdragon/tree-sitter-perl" "v1.1.0"))
    (protobuf . ("https://github.com/mitchellh/tree-sitter-proto"))
    (python . ("https://github.com/tree-sitter/tree-sitter-python" "v0.21.0"))
    (sql . ("https://github.com/m-novikov/tree-sitter-sql"))
    (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.4"))
    (tsquery . ("https://github.com/tree-sitter-grammars/tree-sitter-query" "v0.3.0"))
    (yaml . ("https://github.com/ikatyang/tree-sitter-yaml" "v0.5.0")))
  "List of GIT repositories to build tree-sitter grammars from.")

(defun my-ts--extend-spec (spec suffix)
  "Set custom compilers from SUFFIX for SPEC."
  (let ((len (length spec)))
    (when (or (> len 4) (< len 2))
      (error "Grammar spec for %s is malformed" (car spec)))
    (append spec (nthcdr (- len 2) suffix))))

(require 'treesit)

(defun my-ts-register-grammar-libraries (user-specs)
  "Add USER-SPECS with custom compilers to `treesit-language-source-alist'."
  (let ((suffix `("HEAD" "src" ,my-ts-c-compiler ,my-ts-cpp-compiler)))
    (dolist (spec user-specs)
      (unless (assq (car spec) treesit-language-source-alist)
        (add-to-list 'treesit-language-source-alist
                     (my-ts--extend-spec spec suffix))))))

(with-eval-after-load 'treesit
  (my-ts-register-grammar-libraries my-ts-language-source-alist))

(package-require 'treesit-auto)
(global-treesit-auto-mode t)

(setq treesit-font-lock-level 4)

(provide 'init-tree-sitter)

;;; init-tree-sitter.el ends here
