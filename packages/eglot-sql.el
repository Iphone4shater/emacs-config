;;; eglot-sql.el --- Eglot bindings the to sqls language server -*- lexical-binding: t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1
;; Package-Requires: ()

;;; Commentary:
;;; Code:

(provide 'eglot-sql)

;;; eglot-sql.el ends here
