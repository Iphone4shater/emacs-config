;;; init-cc.el --- Configuration for C-family language modes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'init-prog)

(sp-local-pair 'c++-mode "/*" "*/")

(provide 'init-cc)

;;; init-cc.el ends here
