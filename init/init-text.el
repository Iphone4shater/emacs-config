;;; init-text.el --- Setup for modes derived from `text-mode' -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'init-eglot)

(defun my-text-mode-essential-hook ()
  "Set up `text-mode` essential configuration."
  (visual-line-mode t))

(add-hook 'text-mode-hook #'my-text-mode-essential-hook)

;; (use-package eglot-ltex
;;   :after eglot
;;   :defines (eglot-ltex-server-path eglot-ltex-communication-channel)
;;   :config
;;   (setq eglot-ltex-server-path (expand-file-name "~/.local/tmp/ltex-ls-15.2.0/")
;;         eglot-ltex-communication-channel 'stdio))

(provide 'init-text)

;;; init-text.el ends here
