;;; vc-arc.el --- VC backend for arc -*- lexical-binding: t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1
;; Package-Requires: (cl-lib f subr)

;;; Commentary:

;; This package implements a VC backend for the arc VCS.

;;; Code:

(eval-when-compile
  (require 'cl-lib))

(require 'f)
(require 'subr-x)

(defconst vc-arc--default-command-buffer "*vc*")

(defconst vc-arc--repository-root-file ".arcadia.root"
  "Name of a special file denoting a repository root.")

(defconst vc-arc-program "arc" "Arc executable name.")

(defun vc-arc-root (file)
  "Return arc repository root for FILE if inside it."
  (locate-dominating-file file vc-arc--repository-root-file))

(defun vc-arc--call (buffer command &rest args)
  (apply #'process-file vc-arc-program nil buffer nil command args))

(defun vc-arc--out-ok (command &rest args)
  (zerop (apply #'vc-arc--call '(t nil) command args)))

(defun vc-arc--run-command-string (file &rest args)
  (when file
    (setq args (append args (list (file-relative-name file)))))

  (let* ((ok t)
         (str (with-output-to-string
                (with-current-buffer standard-output
                  (unless (apply #'vc-arc--out-ok args)
                    (setq ok nil))))))
    (and ok str)))

(defun vc-arc-revision-granularity () 'repository)

(defun vc-arc-registered (file)
  "Check whether FILE is registered with arc."
  (when-let* ((file (file-relative-name file))
              (output (vc-arc--run-command-string nil "ls-files" file)))
    (string-equal file (string-trim output))))

(defun vc-arc--status-to-vc-state (code-list)
  "Convert CODE-LIST to a VC status."
  (pcase code-list
    ('nil 'up-to-date)
    (`(,code)
     (pcase code
       ("!!" 'ignored)
       ("??" 'unregistered)
       ("AD" 'removed)
       (_ (cond
           ((string-match-p "^[ RD]+$" code) 'removed)
           ((string-match-p "^[ M]+$" code) 'edited)
           ((string-match-p "^[ A]+$" code) 'added)
           ((string-match-p "^[ U]+$" code) 'conflict)
           (t 'edited)))))
    ('("D " "??") 'unregistered)
    (_ 'edited)))

(defun vc-arc-state (file)
  (let* ((args
          '("status" "--short" "--no-sync-status"
            "--no-bisect-status" "-u" "all"))
         (status (apply #'vc-arc--run-command-string file args)))
    (if (null status)
        'unregistered
      (vc-arc--status-to-vc-state
       (mapcar (lambda (s)
                 (substring s 0 2))
               (split-string status "\0" t))))))

(defun vc-arc-working-revision (_file)
  (when-let ((output (vc-arc--run-command-string nil "rev-parse" "HEAD")))
    (string-trim output)))

(defun vc-arc-checkout-model (_files)
  'implicit)

(defun vc-arc-create-repo ()
  (error "Not implemented"))

(defun vc-arc-command (buffer okstatus file-or-list &rest flags)
  (apply #'vc-do-command
         (or buffer vc-arc--default-command-buffer)
         okstatus vc-arc-program file-or-list flags))

(defun vc-arc-register (files &optional _comment)
  (vc-arc-command nil 0 files "add"))

(declare-function log-edit-extract-headers "log-edit" (headers string))

(defun vc-arc-checkin (files comment &optional _rev)
  (apply #'vc-do-command
         nil 0 files "commit" "-m"
         (log-edit-extract-headers nil comment)))

(defun vc-arc-find-revision (file rev buffer)
  (vc-arc-command
   buffer 0 nil
   "show" "--no-color" "-q"
   (concat (or rev "HEAD") ":" file)))

(defun vc-arc-checkout (file &optional rev)
  (vc-arc-command nil 0 file "checkout" (or rev "HEAD")))

(defun vc-arc-revert (file &optional _)
  (vc-arc-command nil 0 file "reset" "-q" "--")
  (vc-arc-command nil nil file "checkout" "-q" "--"))

(declare-function vc-setup-buffer "vc-dispatcher" (buffer))

(defun vc-arc-print-log (files buffer &optional shortlog start-revision limit)
  (vc-setup-buffer buffer)
  (with-current-buffer buffer
    (apply #'vc-arc-command buffer
           'async files
           (append
            '("log" "--no-color" "--follow")
            (when shortlog
              '("--graph" "--oneline"))
            (when (numberp limit)
              (list "--max-count" (format "%s" limit)))
            (when start-revision
              (if (and limit (not (numberp limit)))
                  (list (concat start-revision ".."
                                (if (equal limit "") "HEAD" limit)))
                (list start-revision)))))))

(defun vc-arc-log-outgoing (buffer remote-location)
  (vc-setup-buffer buffer)
  (vc-arc-command
   buffer 'async nil
   "log"
   "--no-color" "--graph" "--oneline"
   (concat
    (if (string= remote-location "")
        "@{upstream}"
      remote-location)
    "...HEAD")))

(defun vc-arc-log-incoming (buffer remote-location)
  (vc-setup-buffer buffer)
  (vc-arc-command nil 0 nil "fetch" (unless (string= remote-location "")
                                      (replace-regexp-in-string
                                       "/.*" "" remote-location)))
  (vc-arc-command
   buffer 'async nil
   "log"
   "--no-color" "--graph" "--oneline"
   (concat "HEAD.." (if (string= remote-location "")
                        "@{upstream}"
                      remote-location))))

(defun vc-arc-diff (files &optional rev1 rev2 buffer async) )

;;; vc-arc.el ends here
