;;; init-eglot.el --- Embedded LSP client configuration and settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'init-prog)

;; Eglot setup
(defun my-eglot-custom-settings ()
  "Customize `eglot' behavior."
  (when (and (eglot-managed-p) (derived-mode-p 'prog-mode))
    (eglot-inlay-hints-mode -1)))

(require 'eglot)
(add-hook 'eglot-managed-mode-hook #'my-eglot-custom-settings)

(setq eldoc-echo-area-use-multiline-p nil
      eglot-extend-to-xref t)

(require 'auto-package)
(package-require 'consult-eglot)

(define-keymap :keymap my-universal-prog-mode-map
  "r" #'eglot-rename
  "f" #'eglot-format
  "a" #'eglot-code-actions
  "t" #'eglot-find-typeDefinition
  "i" #'eglot-find-implementation
  "d" #'eglot-find-declaration
  "w" #'consult-eglot-symbols)

(provide 'init-eglot)

;;; init-eglot.el ends here
