;;; init-lisp.el --- Configuration for Lisp language family modes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'init-prog)

(sp-with-modes 'lisp-data-mode
  (sp-local-pair "'" nil :actions nil)
  (sp-local-pair "`" "'" :when '(sp-in-string-p sp-in-comment-p)))

(require 'auto-package)

(package-require 'rainbow-delimiters)
(package-require 'aggressive-indent)
(package-require 'lisp-extra-font-lock)

(defun my-lisp-data-mode-hook ()
  "Customize modes derived from `lisp-data-mode'."
  (rainbow-delimiters-mode-enable)
  (aggressive-indent-mode t)
  (lisp-extra-font-lock-mode t))

(add-hook 'lisp-data-mode-hook #'my-lisp-data-mode-hook)

(package-require 'slime)
(setq slime-lisp-implementations
      '((sbcl ("/usr/bin/sbcl"))))

(provide 'init-lisp)

;;; init-lisp.el ends here
