;;; arc-url.el --- Generate an Arcanum link to the code at point -*- lexical-binding: t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1
;; Package-Requires: (url f)

;;; Commentary:

;; This package provides a primitive to generate an Arcanum link to the code
;; at point inside an arc repository.

;;; Code:

(require 'url)
(require 'f)

(defconst arc-url--repository-root-file ".arcadia.root"
  "Name of a special file denoting a repository root.")

(defconst arc-url--arcanum-base-url "https://a.yandex-team.ru/arcadia"
  "Arcanum UI URL.")

(defvar-local arc-url--repository-root nil
  "Arc repository root for the current file.")

(defun arc-url--get-root (path)
  "Get an arc repository root for PATH if it has one."
  (unless (local-variable-p 'arc-url--repository-root)
    (setf arc-url--repository-root
          (locate-dominating-file path arc-url--repository-root-file)))
  arc-url--repository-root)

(defun arc-url--build-url (path linenum)
  "Build an Arcanum URL to a line LINENUM of a file at PATH."
  (let ((url (url-generic-parse-url arc-url--arcanum-base-url)))
    (with-slots (filename) url
      (setf filename (concat (f-join filename path) "?#L" linenum)))
    (url-recreate-url url)))

;;;###autoload
(defun arc-url-echo ()
  "Generate a link to the code at point."
  (interactive)
  (let ((path (buffer-file-name)))
    (unless path
      (error "Buffer is not visiting a file"))
    (let ((root (arc-url--get-root path)))
      (unless root
        (error "File is not under arc version control: %s" path))
      (message "%s" (arc-url--build-url
                     (f-relative path root)
                     (format-mode-line "%l"))))))

(provide 'arc-url)

;;; arc-url.el ends here
