;;; init-visual.el --- Visual settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(global-hl-line-mode t)

(display-battery-mode t)
(display-time-mode t)

;; Prune unused buffers at midnight
(midnight-mode t)

(mouse-avoidance-mode 'banish)
(set-default-coding-systems 'utf-8)

;; Simplify yes-or-no dialogs (don't want to write "yes" or "no" words explicitly)
(fset #'yes-or-no-p #'y-or-n-p)

(require 'fringe)
(setq display-time-day-and-date t
      fringe-mode 2)

(setq-default fill-column 90)

(require 'auto-package)

;; Jumping around with ease
(package-require 'avy)

(keymap-global-unset "M-g c")

(define-keymap :keymap goto-map
  "s" #'avy-goto-char-in-line
  "l" #'avy-goto-line
  "w" #'avy-goto-word-1
  "c l" #'avy-copy-line
  "m l" #'avy-move-line
  "c r" #'avy-copy-region
  "m r" #'avy-move-region
  "k l" #'avy-kill-whole-line
  "k r" #'avy-kill-region)

(setq avy-all-windows 'all-frames)

(package-require 'kaolin-themes)

(provide 'init-visual)

;;; init-visual.el ends here
