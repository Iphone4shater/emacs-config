;;; init-security.el --- Security and cryptography settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'epa)

(with-eval-after-load 'epa
  (setq epg-pinentry-mode 'loopback))

(provide 'init-security)

;;; init-security.el ends here
