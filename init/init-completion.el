;;; init-completion.el --- Custom completion features -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)

(setq completion-ignore-case t
      read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t
      register-preview-delay 0.5)

(package-require 'consult)

(require 'consult-compile)
(require 'consult-flymake)
(require 'consult-imenu)
(require 'consult-info)
(require 'consult-register)
(require 'consult-xref)
(require 'xref)

(setq
 register-preview-function #'consult-register-format
 xref-show-xrefs-function #'consult-xref
 xref-show-definitions-function #'consult-xref
 completion-in-region-function #'consult-completion-in-region
 completion-styles '(partial-completion)
 consult-narrow-key "<"
 consult-preview-key "M-.")

(advice-add #'register-preview :override #'consult-register-window)

(consult-customize
 consult-theme :preview-key '(:debounce 0.2 any)
 consult-ripgrep consult-git-grep consult-grep
 consult-bookmark consult-recent-file consult-xref
 consult--source-bookmark consult--source-file-register
 consult--source-recent-file consult--source-project-recent-file
 :preview-key '(:debounce 0.4 any))

(define-keymap :keymap ctl-x-map
  "<remap> <switch-to-buffer>" #'consult-buffer
  "<remap> <switch-to-buffer-other-window>" #'consult-buffer-other-window
  "<remap> <switch-to-buffer-other-frame>" #'consult-buffer-other-frame
  "<remap> <bookmark-jump>" #'consult-bookmark
  "<remap> <project-list-buffers>" #'consult-project-buffer
  "<remap> <copy-to-register>" #'consult-register-store
  "<remap> <insert-register>" #'consult-register-load
  "r v" #'consult-register)

(keymap-global-set "<remap> <yank-pop>" #'consult-yank-pop)

(define-keymap :keymap goto-map
  "e" #'consult-compile-error
  "f" #'consult-flymake
  "g" #'goto-line
  "i" #'consult-imenu
  "I" #'consult-imenu-multi)

(define-keymap :keymap search-map
  "d" #'consult-find
  "D" #'consult-locate
  "g" #'consult-grep
  "l" #'consult-line
  "m" #'consult-man
  "<remap> <Info-search>" #'consult-info)

(package-require 'vertico)
(vertico-mode t)

(package-require 'marginalia)
(marginalia-mode t)

(provide 'init-completion)

;;; init-completion.el ends here
