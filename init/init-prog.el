;;; init-prog.el --- Basic setup for programming modes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'auto-package)
(package-require 'highlight-numbers)

(defun my-prog-mode-essential-hook ()
  "Set up `prog-mode' essential configuration."
  (display-fill-column-indicator-mode t)
  (display-line-numbers-mode t)
  (flymake-mode t)

  (unless (bound-and-true-p visual-line-mode)
    (setq truncate-lines t))

  (unless (and (fboundp 'treesit-available-p)
	       (treesit-available-p))
    (highlight-numbers-mode t)))

(defun my-clean-whitespace-before-save-if-prog ()
  "Whitespace cleanup hook before saving `prog-mode' buffer."
  (when (derived-mode-p 'prog-mode)
    (delete-trailing-whitespace)))

(add-hook 'prog-mode-hook #'my-prog-mode-essential-hook)
(add-hook 'before-save-hook
          #'my-clean-whitespace-before-save-if-prog)

(defvar my-universal-prog-mode-map (make-sparse-keymap)
  "Keymap for essential commands inside `prog-mode'-derived modes.")

(keymap-set prog-mode-map "C-c l" my-universal-prog-mode-map)

(require 'flymake)

(define-keymap :keymap flymake-mode-map
  "M-n" #'flymake-goto-next-error
  "M-p" #'flymake-goto-prev-error)

(setq flymake-mode-line-format
      '(" " flymake-mode-line-exception flymake-mode-line-counters))

(package-require 'smartparens)
(smartparens-global-strict-mode t)

;; Software documentation provider
(package-require 'devdocs)

;; Source code coverage
(package-require 'cov)

(package-require 'hl-todo)
(package-require 'consult-todo)

(keymap-set goto-map "t" #'consult-todo)

(provide 'init-prog)

;;; init-prog.el ends here
