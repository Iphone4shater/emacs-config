;;; auto-package.el --- Routines for automatic package installation -*- lexical-binding:t -*-

;; Author: Igor Balduev <igor.balduev@protonmail.com>
;; Version: 0.1
;; Package-Requires: (package)

;;; Commentary:

;; This package is intended to streamline package installation when deploying
;; personal Emacs configuration on a new machine.

;;; Code:

(require 'package)

(defun auto-package--ensure-package-installed (package)
  "Install given PACKAGE."
  (unless (package-installed-p package)
    (unless (assoc package package-archive-contents)
      (package-refresh-contents))
    (if (assoc package package-archive-contents)
	    (package-install package)
      (error "Package %s is not available" package)))
  (package-installed-p package))

(defmacro package-require (package)
  "Require PACKAGE, installing it when needed."
  `(progn
     (auto-package--ensure-package-installed ,package)
     (require ,package)))

(provide 'auto-package)

;;; auto-package.el ends here
